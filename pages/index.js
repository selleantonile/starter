import * as React from "react";
import { useEffect, useState } from "react";
import moment from "moment";

const Field = ({ label, value }) => (
  <div style={{ width: "200px", display: "flex", flexDirection: "column" }}>
    <div style={{ color: "gray" }}>{label}</div>
    <div>{value}</div>
  </div>
);

const Card = ({ date, amount, location }) => (
  <div
    style={{
      background: "#fff",
      boxShadow: "0px 0px 7px 1px rgba(0, 0, 0, 0.2)",
      padding: "20px",
      margin: "20px",
      width: "500px",
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    }}
  >
    <Field label="Date" value={moment(date).format("DD MMM YYYY")} />
    <Field label="Amount" value={amount} />
    <Field
      label="Location"
      value={
        (location?.city && `${location.city} (${location.country})`) || "NA"
      }
    />
  </div>
);

export default function Index() {
  const [json, setJson] = useState(undefined);

  const hasAuthorization = (entry) => entry.authorization !== null;

  useEffect(() => {
    fetch("http://localhost:3000/transactions")
      .then((response) => response.json())
      .then((json) => setJson(json));
  }, []);

  return (
    <div style={{ background: "#f7f7f7" }}>
      {json &&
        json.transactions?.map(({ authorization, clearings }) => {
          const location = hasAuthorization ? authorization?.location : "N/A";
          if (clearings.length === 0) {
            const amount = authorization.amount;
            const date = authorization.date;
            return <Card location={location} amount={amount} date={date} />;
          } else {
            return clearings.map((clearing) => (
              <Card
                location={location}
                amount={clearing?.amount}
                date={clearing?.date}
              />
            ));
          }
        })}
    </div>
  );
}
